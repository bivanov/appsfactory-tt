#  Test task for AppsFactory

by Bogdan Ivanov (bogdanivanov at live.com)

## Pre-requisites

* XCode with Swift 5 support (11 or later preferable)
* Cocoapods

## How to build

If you don't have .xcworkspace file near this README, then you have to run `pod install` there. Otherwise you may want to run `pod update`. 

After that open AppsFactory.xcworkspace, build an application for prefered simulator or device and run.

## Notice

Project depends on Last.fm API secret key to be present in Secret.plist. If you've obtained the project in zipped form, then you potentially should have one. Please keep in mind that, despite test account was used to obtain this key from my side, it is still personal and to some degree vulnerable information, so you're advised not to expose it to any third party.

If you obtained the project via git or there is no Secret.plist file inside it for some reason, you have to create it by yourself. You may find some hints on format in Settings.swift file.

## Legal

Project is released under GPLv3 license. 
Copyright © 2019 Bogdan Ivanov


