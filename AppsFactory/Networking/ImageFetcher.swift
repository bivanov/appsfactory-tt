//
//  ImageFetcher.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class ImageFetcher {
    let imageCache: AutoPurgingImageCache
    
    init() {
        imageCache = AutoPurgingImageCache(memoryCapacity: 100_000_000,
                                           preferredMemoryUsageAfterPurge: 60_000_000)
    }
    
    func getImageFor(urlString: String, callback: @escaping ((String, UIImage)?) -> Void) {
        if let image = imageCache.image(withIdentifier: urlString) {
            callback((urlString, image))
            return
        }
        
        request(urlString).responseImage { [unowned self] response in
            if let image = response.result.value {
                self.imageCache.add(image, withIdentifier: urlString)
                callback((urlString, image))
            } else {
                callback(nil)
            }
        }
    }
}
