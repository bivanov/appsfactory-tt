//
//  RequestResult.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

class RequestResult<T>: Codable where T: Codable {
    var results: T
}

typealias ArtistSearchResult = RequestResult<ArtistSearch>
