//
//  Network.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkError: LocalizedError {
    case percentEncodingError(value: String)
    
    var errorDescription: String? {
        switch self {
        case let .percentEncodingError(value):
            return "Was not able to add percent encoding to string: \(value)"
        }
    }
}

class Network {
    private var apiKey: String
    private var rootUrlString = "http://ws.audioscrobbler.com/2.0/"
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    private func buildUrlString(_ text: String) -> String {
        return rootUrlString + text + "&api_key=\(apiKey)&format=json"
    }
    
    func requestArtistsSearch(_ text: String,
                              _ callback: @escaping (ArtistSearchResult?, Error?) -> Void) {
        guard let encodedText = text.addingPercentEncodingForRFC3986() else {
            callback(nil, NetworkError.percentEncodingError(value: text))
            return
        }
        
        let urlString = buildUrlString("?method=artist.search&artist=\(encodedText)")
        fireRequest(urlString: urlString, callback)
    }
    
    func requestAlbumSearch(artistName: String,
                            _ callback: @escaping (AlbumSearch?, Error?) -> Void) {
        guard let encodedArtistName = artistName.addingPercentEncodingForRFC3986() else {
            callback(nil, NetworkError.percentEncodingError(value: artistName))
            return
        }
        
        let urlString = buildUrlString("?method=artist.gettopalbums&artist=\(encodedArtistName)")
        fireRequest(urlString: urlString, callback)
    }
    
    func requestAlbumDetails(albumName: String,
                             artistName: String,
                             _ callback: @escaping (AlbumDetails?, Error?) -> Void) {
        guard let encodedArtistName = artistName.addingPercentEncodingForRFC3986() else {
            callback(nil, NetworkError.percentEncodingError(value: artistName))
            return
        }
        
        guard let encodedAlbumName = albumName.addingPercentEncodingForRFC3986() else {
            callback(nil, NetworkError.percentEncodingError(value: albumName))
            return
        }
        
        let urlString
            = buildUrlString("?method=album.getinfo&artist=\(encodedArtistName)&album=\(encodedAlbumName)")
        fireRequest(urlString: urlString, callback)
    }
    
    private func fireRequest<T>(urlString: String,
                                _ callback: @escaping (T?, Error?) -> Void) where T: Decodable {
        request(urlString).response { response in
            do {
                let result
                    = try JSONDecoder().decode(T.self, from: response.data!)
                callback(result, nil)
            } catch {
                do {
                    let responseError = try JSONDecoder().decode(ResponseError.self, from: response.data!)
                    callback(nil, responseError)
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
}
