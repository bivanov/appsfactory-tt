//
//  CDProvider.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import CoreData

protocol CDProviderDelegate: class {
    func albumsChanged(currentFetchedAlbums: [Album])
    func albumsFetchedFromStore(_ albums: [Album], _ error: Error?)
}

enum CDProviderError: Error, LocalizedError {
    case noAlbumArtist
    
    var errorDescription: String? {
        switch self {
        case .noAlbumArtist:
            return "Album has no artist: currently such albums cannot be deleted from UI"
        }
    }
}

class CDProvider {
    private let context: NSManagedObjectContext
    
    private(set) var fetchedAlbums = Set<Album>()
    
    private var isStoredLocallyCache = NSCache<Album, NSNumber>()
    
    weak var delegate: CDProviderDelegate?
    
    init(context: NSManagedObjectContext) {
        self.context = context
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self,
                                       selector: #selector(managedObjectContextObjectsDidChange),
                                       name: .NSManagedObjectContextObjectsDidChange,
                                       object: context)
    }
    
    private func createCdModelFrom(track: Track) -> CDTrack {
        let cdTrack
            = NSEntityDescription.insertNewObject(forEntityName: "CDTrack",
                                                  into: context) as! CDTrack
        
        cdTrack.name = track.name
        cdTrack.duration = track.duration
        
        return cdTrack
    }
    
    private func createCdModelFrom(album: Album) -> CDAlbum {
        let cdAlbum
            = NSEntityDescription.insertNewObject(forEntityName: "CDAlbum",
                                                  into: context) as! CDAlbum
        
        cdAlbum.id = album.id
        cdAlbum.name = album.name
        cdAlbum.releaseDate = album.releaseDate
        
        return cdAlbum
    }
    
    private func createCdModelFrom(artist: Artist) -> CDArtist {
        let cdArtist
            = NSEntityDescription.insertNewObject(forEntityName: "CDArtist",
                                                  into: context) as! CDArtist
        
        cdArtist.id = artist.id
        cdArtist.name = artist.name
        
        return cdArtist
    }
    
    private func createCdImageFrom(image: ImageInfo) -> CDImage {
        let cdImage
            = NSEntityDescription.insertNewObject(forEntityName: "CDImage",
                                                  into: context) as! CDImage
        
        cdImage.urlString = image.urlString
        cdImage.size = image.size.rawValue
        
        return cdImage
    }
    
    func requestAllAlbums() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CDAlbum")
        
        context.perform { [unowned self] in
            do {
                let result = (try self.context.fetch(fetchRequest)) as! [CDAlbum]
                self.fetchedAlbums = Set<Album>(result.compactMap { cdAlbum -> Album? in
                    let retVal = Album(managedObject: cdAlbum)
                    
                    if let cdArtist = cdAlbum.artist,
                        retVal != nil {
                        let artist = Artist(managedObject: cdArtist)
                        artist?.add(album: retVal!)
                    }
                    
                    return retVal
                })
                self.delegate?.albumsFetchedFromStore([Album](self.fetchedAlbums), nil)
            } catch {
                self.delegate?.albumsFetchedFromStore([], error)
            }
        }
    }
    
    func albumStoredLocally(_ album: Album) -> Bool {
        if let value = isStoredLocallyCache.object(forKey: album)?.boolValue {
            return value
        }
        
        let fetched = fetchedAlbums.contains(album)
        isStoredLocallyCache.setObject(NSNumber(value: fetched), forKey: album)
        return fetched
    }
    
    func fullInfo(for album: Album) -> Album {
        if let first = fetchedAlbums.filter({ anotherAlbum -> Bool in
            anotherAlbum.name == album.name &&
                anotherAlbum.artist?.name == album.artist?.name
        }).first {
            return first
        } else {
            return album
        }
    }
    
    func save(album: Album) -> Error? {
        
        var caughtError: Error?
        
        context.performAndWait {
            let cdAlbum = createCdModelFrom(album: album)
            for track in album.tracks {
                let cdTrack = createCdModelFrom(track: track)
                cdTrack.album = cdAlbum
            }
            
            for image in album.images {
                let cdImage = createCdImageFrom(image: image)
                cdImage.album = cdAlbum
            }
            
            if let artist = album.artist {
                let cdArtist = createCdModelFrom(artist: artist)
                cdAlbum.artist = cdArtist
                
                for image in artist.images {
                    let cdImage = createCdImageFrom(image: image)
                    cdImage.artist = cdArtist
                }
            }
            
            do {
                try context.save()
            } catch {
                caughtError = error
            }
        }
        
        return caughtError
    }
    
    func remove(album: Album) -> Error? {
        guard let artist = album.artist else {
            return CDProviderError.noAlbumArtist
        }
        
        var caughtError: Error?
        
        context.performAndWait {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CDAlbum")
            let predicate = NSPredicate(format: "name = %@ AND artist.name = %@", album.name, artist.name)
            fetchRequest.predicate = predicate

            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            batchDeleteRequest.resultType = .resultTypeObjectIDs

            do {
                let result = try context.execute(batchDeleteRequest) as! NSBatchDeleteResult

                let changes: [AnyHashable: Any] = [
                    NSDeletedObjectsKey: result.result as! [NSManagedObjectID]
                ]
                NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes,
                                                    into: [context])

            } catch {
                caughtError = error
            }
        }
        
        return caughtError
    }
    
    @objc func managedObjectContextObjectsDidChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        
        var changed = false
        
        // NOTE: better look for inserts and updates from all Artists, then look for albumes changes, gather it
        // and send to delegate who will store a set, not an array
        
        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> {
            for insert in inserts {
                if let insert = insert as? CDAlbum,
                    let album = Album(managedObject: insert) {
                    
                    if let cdArtist = insert.artist {
                        let artist = Artist(managedObject: cdArtist)
                        artist?.add(album: album)
                    }
                    
                    changed = true
                    fetchedAlbums.insert(album)
                }
            }
        }
        
        if let deleted = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> {
            for deletion in deleted {
                if let deletion = deletion as? CDAlbum,
                    let album = Album(managedObject: deletion) {
                    
                    if let cdArtist
                        = deletion.changedValuesForCurrentEvent()["artist"] as? CDArtist {
                        let artist = Artist(managedObject: cdArtist)
                        artist?.add(album: album)
                    }

                    changed = true
                    
                    fetchedAlbums.remove(album)
                }
            }
        }
        
        isStoredLocallyCache.removeAllObjects()
        
        if changed {
            delegate?.albumsChanged(currentFetchedAlbums: [Album](fetchedAlbums))
        }
    }
}
