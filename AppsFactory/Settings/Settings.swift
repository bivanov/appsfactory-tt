//
//  Settings.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

class Settings {
    private var settingsDict: NSDictionary?
    
    var lastFmApiKey: String? {
        return settingsDict?["LastFM_API_Key"] as? String
    }
    
    init(plistFileName: String, bundle: Bundle = Bundle.main) {
        if let path = bundle.path(forResource: plistFileName, ofType: ".plist") {
            settingsDict = NSDictionary(contentsOfFile: path)
        }
    }
}
