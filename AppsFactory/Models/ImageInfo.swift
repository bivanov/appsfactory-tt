//
//  ImageInfo.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

enum ImageSize: String, Codable {
    case small
    case medium
    case large
    case extralarge
    case mega
    case invalid
    
    init(rawValue: String) {
        switch rawValue {
        case "small":
            self = .small
        case "medium":
            self = .medium
        case "large":
            self = .large
        case "extralarge":
            self = .extralarge
        default:
            self = .invalid
        }
    }
}

class ImageInfo: Codable, ManagedObjectInitialisible {
    typealias T = CDImage
    
    let urlString: String
    let size: ImageSize
    
    enum CodingKeys: String, CodingKey {
        case urlString = "#text"
        case size
    }
    
    required init?(managedObject: CDImage) {
        urlString = managedObject.urlString!
        size = ImageSize(rawValue: managedObject.size!)
    }
    
    static func mostAppropriateMediumSize(_ set: [ImageInfo]?) -> ImageInfo? {
        let sizes: [ImageSize] = [.large, .medium, .small]
        return ImageInfo.mostAppropriateSize(set, sizes)
    }
    
    static func mostAppropriateBigSize(_ set: [ImageInfo]?) -> ImageInfo? {
        let sizes: [ImageSize] = [.mega, .extralarge, .large]
        return ImageInfo.mostAppropriateSize(set, sizes)
    }
    
    private static func mostAppropriateSize(_ set: [ImageInfo]?,
                                            _ sizes: [ImageSize]) -> ImageInfo? {
        guard let set = set else {
            return nil
        }
 
        for size in sizes {
            if let image = set.filter({ (info) -> Bool in
                return info.size == size
            }).first {
                return image
            }
        }
        
        return nil
    }
}
