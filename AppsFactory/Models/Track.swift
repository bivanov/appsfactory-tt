//
//  Track.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

class Track: Codable, ManagedObjectInitialisible {
    typealias T = CDTrack
    
    let name: String
    let duration: String?
    var album: Album?
    
    enum CodingKeys: String, CodingKey {
        case name
        case duration
    }
    
    required init?(managedObject: CDTrack) {
        name = managedObject.name!
        duration = managedObject.duration
    }
}
