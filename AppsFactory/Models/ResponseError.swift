//
//  ResponseError.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

class ResponseError: Error, Codable, LocalizedError {
    var errorCode: Int
    var message: String
    
    enum CodingKeys: String, CodingKey {
        case errorCode = "error"
        case message
    }
    
    var errorDescription: String? {
        return message
    }
}
