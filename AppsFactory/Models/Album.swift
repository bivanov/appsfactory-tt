//
//  Album.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

class AlbumSearch: Codable {
    enum CodingKeys: String, CodingKey {
        case albums = "topalbums"
    }
    
    class AlbumList: Codable {
        enum CodingKeys: String, CodingKey {
            case albums = "album"
        }
        
        var albums: [Album]
    }
    
    var albums: AlbumList
}

class AlbumDetails: Codable {
    var album: Album
}

/// Indicates about album fetch origin
enum AlbumFetchOrigin {
    /// Album instance was fetched by network request
    case network
    
    /// Album instance was fetched from CoreData store
    case storage
}

class Album: Codable, ManagedObjectInitialisible, Equatable, Hashable,
    CustomStringConvertible {
    
    typealias T = CDAlbum
    
    let id: String?
    let name: String
    let releaseDate: String?
    private(set) var tracks: [Track] = []
    let images: [ImageInfo]
        
    /// Fetch origin.
    ///
    /// - NOTE: This property just indicates whether particular album instance was received
    /// by network request or from CoreData store. Even if this property is
    /// set to `.network`, it doesn't mean that album is not stored in CoreData.
    ///
    /// To get such information, please use `CDProvider`'s `albumStoredLocally(:)`
    var fetchOrigin: AlbumFetchOrigin
    
    enum CodingKeys: String, CodingKey {
        case name
        case releaseDate = "releasedate"
        case id = "mbid"
        case images = "image"
        case tracks
    }
    
    enum TrackCodingKeys: String, CodingKey {
        case track
    }
    
    var artist: Artist?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        releaseDate = try container.decodeIfPresent(String.self, forKey: .releaseDate)
        images = try container.decodeIfPresent([ImageInfo].self, forKey: .images) ?? []
        if let trackContainer = try? container.nestedContainer(keyedBy: TrackCodingKeys.self,
                                                               forKey: .tracks) {
            tracks = try trackContainer.decode([Track].self, forKey: .track)
        }
        fetchOrigin = .network
    }
    
    required init?(managedObject: CDAlbum) {
        id = managedObject.id
        name = managedObject.name!
        releaseDate = managedObject.releaseDate
        fetchOrigin = .storage
        
        tracks = managedObject.tracks?.compactMap({ track -> Track? in
            if let track = track as? CDTrack {
                return Track(managedObject: track)
            }
            
            return nil
        }) ?? []
        
        images = managedObject.images?.compactMap({ image -> ImageInfo? in
            if let image = image as? CDImage {
                return ImageInfo(managedObject: image)
            }
            
            return nil
        }) ?? []
        
        tracks.forEach { track in
            track.album = self
        }
    }
    
    func add(track: Track) {
        tracks.append(track)
        track.album = self
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(artist?.name)
    }
    
    static func ==(lhs: Album, rhs: Album) -> Bool {
        return lhs.name == rhs.name && lhs.artist?.name == rhs.artist?.name
    }
    
    var description: String {
        return "[Album]: \(name), tracks count \(tracks.count), artist: \(String(describing: artist?.name))"
    }
}
