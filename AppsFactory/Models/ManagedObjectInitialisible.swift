//
//  ManagedObjectInitialisible.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import CoreData

protocol ManagedObjectInitialisible {
    associatedtype T: NSManagedObject
    
    init?(managedObject: T)
}
