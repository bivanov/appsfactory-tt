//
//  Artist.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

class ArtistSearch: Codable {
    enum CodingKeys: String, CodingKey {
        case artistMatches = "artistmatches"
    }
    
    class ArtistList: Codable {
        enum CodingKeys: String, CodingKey {
            case artists = "artist"
        }
        
        var artists: [Artist]
    }
    
    var artistMatches: ArtistList
}

class Artist: Hashable, Codable, ManagedObjectInitialisible {

    let name: String
    let id: String
    let images: [ImageInfo]
    private(set) var albums: [Album] = []
    
    enum CodingKeys: String, CodingKey {
        case name
        case images = "image"
        case id = "mbid"
    }

    typealias T = CDArtist
    
    required init?(managedObject: CDArtist) {
        id = managedObject.id!
        name = managedObject.name!
        
        albums = managedObject.albums?.compactMap({ album -> Album? in
            if let album = album as? CDAlbum {
                return Album(managedObject: album)
            }
            
            return nil
        }) ?? []
        
        images = managedObject.images?.compactMap({ image -> ImageInfo? in
            if let image = image as? CDImage {
                return ImageInfo(managedObject: image)
            }
            
            return nil
        }) ?? []
        
        albums.forEach { album in
            album.artist = self
        }
    }
    
    func add(album: Album) {
        albums.append(album)
        album.artist = self
    }
    
    static func == (lhs: Artist, rhs: Artist) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}
