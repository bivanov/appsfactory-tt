//
//  ArtistAlbumTableViewCell.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ArtistAlbumTableViewCell: UITableViewCell {

    var imageUrlString: String?
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        coverImageView.layer.cornerRadius = 8.0
    }

    func setStored(_ value: Bool) {
        if nameLabel.text != nil {
            if value {
                nameLabel.text = "💾 " + nameLabel.text!
                nameLabel.textColor = UIColor.gold
            } else {
                nameLabel.text
                    = nameLabel.text!.replacingOccurrences(of: "💾 ", with: "")
                nameLabel.textColor = UIColor.black
            }
        }
    }
}
