//
//  CoverTableViewCell.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/21/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit

class CoverTableViewCell: UITableViewCell {

    @IBOutlet var coverImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
