//
//  StoredAlbumsViewController.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit

class StoredAlbumsViewController: UIViewController, UISearchBarDelegate, CDProviderDelegate, AppDelegateConsumer, StoredAlbumsStrategyDelegate, CustomSwitchDelegate,
    UISearchResultsUpdating, SearchHistoryControllerDelegate {

    var strategies = [StoredAlbumsStrategy]()
    var currentStrategy: StoredAlbumsStrategy!
    
    var customSwitch: CustomSwitch!
    
    lazy var searchHistoryController: SearchHistoryController = {
        let controller = SearchHistoryController(with: hintsTableView)
        controller.delegate = self
        return controller
    }()
    var hintsTableView: UITableView!
    
    @IBOutlet var collectionView: UICollectionView!
    lazy var searchController: UISearchController = {
        let retVal = UISearchController(searchResultsController: nil)
        retVal.searchBar.delegate = self
        retVal.searchResultsUpdater = self
        return retVal
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UICollectionReusableView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: "Header")
        
        strategies.append(AlbumsStoredAlbumsStrategy(imageFetcher: imageFetcher))
        strategies.append(GroupedAlbumsStoredAlbumsStrategy(imageFetcher: imageFetcher))
        
        for strategy in strategies {
            strategy.delegate = self
        }
        
        currentStrategy = strategies.first!
        
        collectionView.delegate = currentStrategy
        collectionView.dataSource = currentStrategy

        let switchFrame = CGRect(x: 0,
                                 y: 0,
                                 width: 70.0,
                                 height: 10.0)
        
        customSwitch = CustomSwitch(frame: switchFrame)
        customSwitch.delegate = self
        
        hintsTableView = UITableView()

        hintsTableView.frame = CGRect(x: view.frame.width * 0.1,
                                      y: 0,
                                      width: view.frame.width * 0.8,
                                      height: view.frame.height / 3)
        hintsTableView.layer.cornerRadius = 9.0
        hintsTableView.layer.borderWidth = 1
        hintsTableView.layer.borderColor = UIColor.wetAsphalt.cgColor
        hintsTableView.backgroundColor = UIColor.beige
        hintsTableView.isUserInteractionEnabled = true
        searchController.view.addSubview(hintsTableView)
        hintsTableView.isHidden = true
        
        navigationItem.leftBarButtonItem
            = UIBarButtonItem(customView: customSwitch)
        
        navigationItem.rightBarButtonItem
            = UIBarButtonItem(barButtonSystemItem: .search,
                              target: self,
                              action: #selector(toggleSearchBar))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cdProvider.delegate = self
        cdProvider.requestAllAlbums()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.searchController?.resignFirstResponder()
        hintsTableView.isHidden = true
        
        cdProvider.delegate = nil
    }
    
    @objc func toggleSearchBar() {
        if navigationItem.searchController != nil {
            navigationItem.searchController = nil
            hintsTableView.isHidden = true
        } else {
            navigationItem.searchController = searchController
        }
        
        navigationController?.view.setNeedsLayout()
        
    }
    
    // MARK: - UISearchBarDelegate methods
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let text = searchBar.text {
            searchHistoryController.searchHistory.add(term: text)
        }
        
        if let vc = storyboard!.instantiateViewController(withIdentifier: "ArtistSearchResultViewController")
            as? ArtistSearchResultViewController {
            
            vc.textToSearch = searchBar.text
            navigationController?.pushViewController(vc, animated: true)
        }
    }
        
    // MARK: - CDProviderDelegate methods
    
    func albumsChanged(currentFetchedAlbums: [Album]) {
        
        currentStrategy.albums = currentFetchedAlbums
        
        DispatchQueue.main.async { [unowned self] in
            self.collectionView.isHidden = self.currentStrategy.albums.count == 0
            self.collectionView.reloadData()
        }
    }
    
    func albumsFetchedFromStore(_ albums: [Album], _ error: Error?) {
        guard error == nil else {
            DispatchQueue.main.async { [unowned self] in
                self.showAlert(title: "Warning", message: error!.localizedDescription)
            }
            return
        }
        
        currentStrategy.albums = albums

        DispatchQueue.main.async { [unowned self] in
            self.collectionView.isHidden = self.currentStrategy.albums.count == 0
            self.collectionView.reloadData()
        }
    }
    
    // MARK: - StoredAlbumsStrategyDelegate methods
    
    func strategySelectedAlbum(_ album: Album) {
        if let vc = storyboard!.instantiateViewController(withIdentifier: "AlbumDetailViewController")
            as? AlbumDetailViewController {

            vc.albumData = album

            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - CustomSwitchDelegate methods
    
    func valueChanged(caller: CustomSwitch, value: CustomSwitchState) {
        let albums = currentStrategy.albums
        
        switch value {
        case .firstOn:
            currentStrategy = strategies[0]
        case .secondOn:
            currentStrategy = strategies[1]
        }
        
        collectionView.dataSource = currentStrategy
        collectionView.delegate = currentStrategy
        
        currentStrategy.albums = albums
        
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                    at: .top,
                                    animated: true)
    }
    
    // MARK: - UISearchResultsUpdating methods
    
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text
        hintsTableView.isHidden = text == nil || text?.count == 0
        
        if !hintsTableView.isHidden {
            searchHistoryController.refreshUi(for: text!)
        }
    }
    
    // MARK: - SearchHistoryControllerDelegate methods
    
    func hintWasSelected(_ hint: String) {
        if let vc = storyboard!.instantiateViewController(withIdentifier: "ArtistSearchResultViewController")
            as? ArtistSearchResultViewController {
            
            vc.textToSearch = hint
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

