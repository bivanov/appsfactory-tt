//
//  AlbumDetailViewController.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit

class AlbumDetailViewController: UIViewController, AppDelegateConsumer, UITableViewDelegate,
    UITableViewDataSource, UIEnabler {
    
    @IBOutlet var tableView: UITableView!
    
    lazy var addAlbumBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                     target: self,
                                                     action: #selector(saveAlbumLocally))
    
    lazy var deleteAlbumBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash,
                                                        target: self,
                                                        action: #selector(deleteAlbumLocally))
    
    var albumData: Album? {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = albumData != nil
        }
    }
    var tracks: [Track] {
        return albumData?.tracks ?? []
    }
    
    var viewsToInteract: [UIView] {
        return [tableView]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if albumData != nil,
            !cdProvider.albumStoredLocally(albumData!) {
            navigationItem.rightBarButtonItem
                = UIBarButtonItem(barButtonSystemItem: .add,
                                  target: self,
                                  action: #selector(saveAlbumLocally))
            navigationItem.rightBarButtonItem = addAlbumBarButtonItem
        } else {
            navigationItem.rightBarButtonItem = deleteAlbumBarButtonItem
        }
        
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let albumData = albumData else {
            return
        }
        
        navigationItem.title = albumData.name
                
        if !cdProvider.albumStoredLocally(albumData) {
            if let artistName = albumData.artist?.name {
                disableViews()
                network.requestAlbumDetails(albumName: albumData.name,
                                            artistName: artistName,
                                            albumDetailCallback)
            }
        } else {
            if tracks.count == 0 {
                self.albumData = cdProvider.fullInfo(for: albumData)
            }
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return tableView.frame.width
        case 1:
            return 28.0
        default:
            return 0.0
        }
    }
    
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return tracks.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.wetAsphalt
        
        let label = UILabel()
        label.text = "by " + (albumData?.artist?.name ?? "unknown artist")
                        + " (\(tracks.count) tracks)"
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        
        label.trailingAnchor
            .anchorWithOffset(to: view.trailingAnchor)
            .constraint(equalToConstant: 8.0).isActive = true
        label.leadingAnchor
            .anchorWithOffset(to: view.leadingAnchor)
            .constraint(equalToConstant: -8.0).isActive = true
        label.centerYAnchor
            .anchorWithOffset(to: view.centerYAnchor)
            .constraint(equalToConstant: 0.0).isActive = true
        
        return view
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        guard section == 1 else {
            return 0.0
        }
        
        return 30.0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cellIdentifier = "CoverTableViewCell"
            
            var cell: CoverTableViewCell?
            
            cell
                = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CoverTableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier) as? CoverTableViewCell
            }
            
            if let albumData = albumData {
                let imageUrls = albumData.images.filter({ info -> Bool in
                    return info.size != .invalid
                })
                
                if let urlString = ImageInfo.mostAppropriateBigSize(imageUrls)?.urlString {
                    imageFetcher.getImageFor(urlString: urlString) { info in
                        if let info = info {
                            DispatchQueue.main.async {
                                cell?.coverImageView.contentMode = .scaleAspectFill
                                cell?.coverImageView.image = info.1
                            }
                        }
                    }
                }
            }
            
            return cell!
        case 1:
            let cellIdentifier = "TrackTableViewCell"
            
            var cell: TrackTableViewCell?
            
            cell
                = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TrackTableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier) as? TrackTableViewCell
            }
            
            let track = tracks[indexPath.row]
            
            cell?.nameLabel.text = track.name
            cell?.durationString = track.duration ?? "0"
            
            return cell!
        default:
            fatalError("Invalid section")
        }
        
        
    }
    
    // MARK: - UIScrollViewDelegate methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexPath = IndexPath(row: 0, section: 0)
        
        let cellHeight = tableView(tableView, heightForRowAt: indexPath)

        let percentage = scrollView.contentOffset.y / cellHeight

        if let cell
            = tableView.cellForRow(at: indexPath) as? CoverTableViewCell {
            cell.coverImageView.alpha = 1.0 - percentage / 2.0
        }
    }
    
    // MARK: -
    
    func albumDetailCallback(result: AlbumDetails?, error: Error?) {
        guard error == nil else {
            DispatchQueue.main.async { [unowned self] in
                self.showAlert(title: "Warning", message: error!.localizedDescription)
                self.enableViews()
            }
            return
        }
        
        guard let album = result?.album else {
            return
        }
        
        DispatchQueue.main.async { [unowned self] in
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            
            for track in album.tracks {
                self.albumData?.add(track: track)
            }
            
            self.tableView.reloadData()
            self.enableViews()
        }
    }
    
    @objc func saveAlbumLocally() {
        guard let albumData = albumData else {
            return
        }
        
        if let error = cdProvider.save(album: albumData) {
            showAlert(title: "Warning", message: error.localizedDescription)
        } else {
            navigationItem.rightBarButtonItem = deleteAlbumBarButtonItem
        }
    }
    
    @objc func deleteAlbumLocally() {
        guard let albumData = albumData else {
            return
        }
        
        if let error = cdProvider.remove(album: albumData) {
            showAlert(title: "Warning", message: error.localizedDescription)
        } else {
            navigationItem.rightBarButtonItem = addAlbumBarButtonItem
        }
    }
}
