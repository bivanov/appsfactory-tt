//
//  ArtistSearchResultViewController.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/18/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit

class ArtistSearchResultViewController: UIViewController, UITableViewDelegate,
    UITableViewDataSource, AppDelegateConsumer, UIEnabler {

    @IBOutlet var tableView: UITableView!
    
    var currentData = [Artist]()
    
    var textToSearch: String? {
        didSet {
            navigationItem.title = textToSearch
        }
    }
    
    var viewsToInteract: [UIView] {
        return [tableView]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.isHidden = true
        
        if textToSearch != nil {
            disableViews()
            network.requestArtistsSearch(textToSearch!,
                                         artistSearchCallback)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard!.instantiateViewController(withIdentifier: "AlbumSearchResultViewController")
            as? AlbumSearchResultViewController {
            
            vc.artistData = currentData[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentData.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ArtistTableViewCell"
        
        var cell: ArtistAlbumTableViewCell?
        
        cell
            = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ArtistAlbumTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier) as? ArtistAlbumTableViewCell
        }
        
        let artistInfo = currentData[indexPath.row]
                
        cell?.coverImageView.image = nil
        
        if let urlString = ImageInfo.mostAppropriateMediumSize(artistInfo.images)?.urlString {
            cell?.imageUrlString = urlString
            imageFetcher.getImageFor(urlString: urlString) { info in
                DispatchQueue.main.async {
                    if cell?.imageUrlString == info?.0 {
                        cell?.coverImageView.contentMode = .scaleAspectFill
                        cell?.coverImageView.image = info?.1
                    }
                }
            }
        }
        
        cell?.nameLabel.text = artistInfo.name
        
        return cell!
    }
    
    func updateCell(at indexPath: IndexPath, with image: UIImage, _ url: String) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    // MARK: -
    
    func artistSearchCallback(result: ArtistSearchResult?, error: Error?) {
        guard error == nil else {
            DispatchQueue.main.async { [unowned self] in
                self.showAlert(title: "Warning", message: error!.localizedDescription)
                self.enableViews()
            }
            return
        }
        
        if result != nil {
            currentData = result!.results.artistMatches.artists
            
            DispatchQueue.main.async { [unowned self] in
                self.tableView.isHidden = false
                self.tableView.reloadData()
                self.enableViews()
            }
        }
    }
}

