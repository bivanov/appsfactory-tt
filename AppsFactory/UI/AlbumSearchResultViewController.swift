//
//  AlbumSearchResultViewController.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit

class AlbumSearchResultViewController: UIViewController, UITableViewDelegate,
    UITableViewDataSource, AppDelegateConsumer, CDProviderDelegate, UIEnabler {

    @IBOutlet var tableView: UITableView!
    
    var currentData = [Album]()
    
    var artistData: Artist? {
        didSet {
            navigationItem.title = artistData?.name
        }
    }
    
    var viewsToInteract: [UIView] {
        return [tableView]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.isHidden = true
        
        cdProvider.delegate = self
        
        if artistData?.name != nil {
            disableViews()
            network.requestAlbumSearch(artistName: artistData!.name,
                                       albumSearchCallback)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        cdProvider.delegate = nil
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard!.instantiateViewController(withIdentifier: "AlbumDetailViewController")
            as? AlbumDetailViewController {
            
            let albumInfo = currentData[indexPath.row]
            artistData?.add(album: albumInfo)
            vc.albumData = albumInfo
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView,
                   editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let albumInfo = currentData[indexPath.row]
        let fetched = cdProvider.albumStoredLocally(albumInfo)
        
        var retVal = [UITableViewRowAction]()
        
        if fetched {
            let deleteAction
                = UITableViewRowAction(style: .destructive, title: "Delete") { [unowned self] _, _ in
                    if let error = self.cdProvider.remove(album: albumInfo) {
                        self.showAlert(title: "Warning", message: error.localizedDescription)
                    } else {
                        self.tableView.reloadData()
                    }
            }
            
            deleteAction.backgroundColor = UIColor.red
            retVal.append(deleteAction)
        } else {
            let addAction
                = UITableViewRowAction(style: .destructive, title: "Add") { [unowned self] _, _ in
                    if let artistName = self.artistData?.name {
                        self.disableViews()
                        self.network.requestAlbumDetails(albumName: albumInfo.name,
                                                         artistName: artistName,
                                                         self.albumDetailCallback)
                    }
            }
            addAction.backgroundColor = UIColor.wetAsphalt
            retVal.append(addAction)
        }
        
        return retVal
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentData.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ArtistTableViewCell"
        
        var cell: ArtistAlbumTableViewCell?
        
        cell
            = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ArtistAlbumTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier) as? ArtistAlbumTableViewCell
        }
        
        let albumInfo = currentData[indexPath.row]
        
        cell?.coverImageView.image = nil

        if let urlString = ImageInfo.mostAppropriateMediumSize(albumInfo.images)?.urlString  {
            cell?.imageUrlString = urlString
            imageFetcher.getImageFor(urlString: urlString) { info in
                DispatchQueue.main.async {
                    if cell?.imageUrlString == info?.0 {
                        cell?.coverImageView.contentMode = .scaleAspectFill
                        cell?.coverImageView.image = info?.1
                    }
                }
            }
        }
        
        cell?.nameLabel.text = albumInfo.name
        
        let fetched = cdProvider.albumStoredLocally(albumInfo)
        
        cell?.setStored(fetched)
        
        return cell!
    }
    
    func updateCell(at indexPath: IndexPath, with image: UIImage, _ url: String) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    // MARK: -
    
    func albumSearchCallback(result: AlbumSearch?, error: Error?) {
        guard error == nil else {
            DispatchQueue.main.async { [unowned self] in
                self.showAlert(title: "Warning", message: error!.localizedDescription)
                self.enableViews()
            }
            return
        }
        
        guard let result = result else {
            return
        }
        
        currentData = result.albums.albums
        currentData.forEach { album in
            album.artist = artistData
        }
        
        DispatchQueue.main.async { [unowned self] in
            self.tableView.isHidden = false
            self.tableView.reloadData()
            self.enableViews()
        }
    }
    
    // MARK: -
    
    func albumDetailCallback(result: AlbumDetails?, error: Error?) {
        guard error == nil else {
            DispatchQueue.main.async { [unowned self] in
                self.showAlert(title: "Warning", message: error!.localizedDescription)
                self.enableViews()
            }
            return
        }
        
        guard let album = result?.album else {
            return
        }
        
        album.artist = artistData
        
        if let error = self.cdProvider.save(album: album) {
            DispatchQueue.main.async { [unowned self] in
                self.showAlert(title: "Warning", message: error.localizedDescription)
                self.enableViews()
            }
        } else {
            DispatchQueue.main.async { [unowned self] in
                self.tableView.reloadData()
                self.enableViews()
            }
        }
    }
    
    // MARK: - CDProviderDelegate methods
    
    func albumsChanged(currentFetchedAlbums: [Album]) {
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    func albumsFetchedFromStore(_ albums: [Album], _ error: Error?) { }
}
