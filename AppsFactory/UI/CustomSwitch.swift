//
//  CustomSwitch.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/21/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import UIKit

enum CustomSwitchState {
    case firstOn
    case secondOn
}

protocol CustomSwitchDelegate: class {
    func valueChanged(caller: CustomSwitch, value: CustomSwitchState)
}

class CustomSwitch: UIButton {
    
    private(set) var switchState: CustomSwitchState = .firstOn {
        didSet {
            guard oldValue != self.switchState else {
                return
            }
            
            self.animateLayer()
        }
    }
    
    private var backgroundLayer: CAShapeLayer!
    
    weak var delegate: CustomSwitchDelegate?
    
    private lazy var leftPath: CGPath
        = UIBezierPath(roundedRect: CGRect(x: 2,
                                           y: 2,
                                           width: frame.height - 4,
                                           height: frame.height - 4),
                       cornerRadius: (frame.height - 2) / 2).cgPath
    
    private lazy var interPath: CGPath
        = UIBezierPath(roundedRect: CGRect(x: 2,
                                           y: 2,
                                           width: frame.width - 2,
                                           height: frame.height - 4),
                       cornerRadius: (frame.height - 2) / 2).cgPath
    
    private lazy var rightPath: CGPath
        = UIBezierPath(roundedRect: CGRect(x: frame.width - frame.height + 2,
                                           y: 2,
                                           width: frame.height - 4,
                                           height: frame.height - 4),
                       cornerRadius: (frame.height - 2) / 2).cgPath
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUi()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUi()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.height / 2
        
        leftPath
            = UIBezierPath(roundedRect: CGRect(x: 2,
                                               y: 2,
                                               width: frame.height - 4,
                                               height: frame.height - 4),
                           cornerRadius: (frame.height - 2) / 2).cgPath
        
        interPath
            = UIBezierPath(roundedRect: CGRect(x: 2,
                                               y: 2,
                                               width: frame.width - 2,
                                               height: frame.height - 4),
                           cornerRadius: (frame.height - 2) / 2).cgPath
        
        rightPath
            = UIBezierPath(roundedRect: CGRect(x: frame.width - frame.height + 2,
                                               y: 2,
                                               width: frame.height - 4,
                                               height: frame.height - 4),
                           cornerRadius: (frame.height - 2) / 2).cgPath
        
        backgroundLayer.path = switchState == .firstOn ? leftPath : rightPath
    }
    
    private func setupUi() {
        
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.wetAsphalt.cgColor
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        
        addTarget(self,
                  action: #selector(switchAction(sender:)),
                  for: .touchUpInside)
        
        setupLayers()
    }
    
    private func setupLayers() {
        backgroundLayer = CAShapeLayer()
        
        backgroundLayer.path = switchState == .firstOn ? leftPath : rightPath
        
        backgroundLayer.fillColor = UIColor.wetAsphalt.cgColor
        
        layer.insertSublayer(backgroundLayer, at: 0)
    }
    
    private func animateLayer() {
        let animation = CAKeyframeAnimation(keyPath: "path")
        animation.duration = 0.4
        
        animation.keyTimes = [0.0, 0.7, 1.0]
        
        if switchState == .secondOn {
            animation.values = [leftPath, interPath, rightPath]
        } else {
            animation.values = [rightPath, interPath, leftPath]
        }
        
        backgroundLayer.path = (animation.values?.last as! CGPath)
        
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.repeatCount = 1
        
        backgroundLayer.add(animation, forKey: "path")
    }
    
    @objc private func switchAction(sender: UIButton) {
        switchState = switchState == .firstOn ? .secondOn : .firstOn
        
        delegate?.valueChanged(caller: self, value: switchState)
    }
    
}
