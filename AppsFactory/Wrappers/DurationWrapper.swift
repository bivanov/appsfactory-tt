//
//  DurationWrapper.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

@propertyWrapper
struct DurationComponent {
    var value: String

    init() {
        value = "0:00"
    }

    var wrappedValue: String {
        get { value }
        set { value = formatString(newValue) }
    }
    
    private func formatString(_ str: String) -> String {
        let intRepresentation = NSString(string: str).integerValue
        
        let minutes = intRepresentation / 60
        let seconds = intRepresentation % 60
        
        return String.init(format: "%d:%02d", minutes, seconds)
    }
    
}
