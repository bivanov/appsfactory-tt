//
//  AppDelegateConsumer.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/19/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation

protocol AppDelegateConsumer {
    var imageFetcher: ImageFetcher { get }
    var network: Network { get }
    var cdProvider: CDProvider { get }
}

extension AppDelegateConsumer {
    var imageFetcher: ImageFetcher {
        return AppDelegate.shared.imageFetcher
    }
    
    var network: Network {
        return AppDelegate.shared.network
    }
    
    var cdProvider: CDProvider {
        return AppDelegate.shared.cdProvider
    }
}
