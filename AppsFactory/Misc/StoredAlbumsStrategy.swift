//
//  StoredAlbumsStrategy.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/22/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import UIKit

protocol StoredAlbumsStrategyDelegate: class {
    func strategySelectedAlbum(_ album: Album)
}

protocol StoredAlbumsStrategy: UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {
    var albums: [Album] { get set }
    var imageFetcher: ImageFetcher { get }
    var delegate: StoredAlbumsStrategyDelegate? { get set }
    init(imageFetcher: ImageFetcher)
}

class BasicStoredAlbumsStrategy: NSObject, StoredAlbumsStrategy {
    var albums: [Album]
    var imageFetcher: ImageFetcher
    var delegate: StoredAlbumsStrategyDelegate?
    
    required init(imageFetcher: ImageFetcher) {
        self.albums = []
        self.imageFetcher = imageFetcher
    }
    
    fileprivate func getAlbum(for indexPath: IndexPath) -> Album {
        return albums[indexPath.row]
    }
    
    // MARK: - UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 2) / 2, height: 104)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        delegate?.strategySelectedAlbum(getAlbum(for: indexPath))
    }
    
    // MARK: - UICollectionViewDataSource methods
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "AlbumCollectionViewCell"
        
        var cell: AlbumCollectionViewCell?
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                  for: indexPath) as? AlbumCollectionViewCell
        
        if cell == nil {
            cell = AlbumCollectionViewCell()
        }
        
        let album = getAlbum(for: indexPath)
        
        cell?.albumNameLabel.text = album.name
        cell?.artistNameLabel.text = album.artist?.name ?? "Unknown artist"
        
        cell?.coverImageView.image = nil
        
        if let urlString = ImageInfo.mostAppropriateMediumSize(album.images)?.urlString {
            cell?.imageUrlString = urlString
            imageFetcher.getImageFor(urlString: urlString) { info in
                DispatchQueue.main.async {
                    if cell?.imageUrlString == info?.0 {
                        cell?.coverImageView.contentMode = .scaleAspectFill
                        cell?.coverImageView.image = info?.1
                    }
                }
            }
        }
        
        return cell!
    }
}

typealias AlbumsStoredAlbumsStrategy = BasicStoredAlbumsStrategy

class GroupedAlbumsStoredAlbumsStrategy: BasicStoredAlbumsStrategy {
    private var groupedAlbums = [[Album]]()
    
    override var albums: [Album] {
        didSet {
            groupedAlbums.removeAll()
            var intermediate = [Artist: [Album]]()
                        
            for album in albums {
                guard let artist = album.artist else {
                    continue
                }
                
                if intermediate.keys.contains(artist) {
                    intermediate[artist]?.append(album)
                } else {
                    intermediate[artist] = [album]
                }
                
            }
            
            let sortedKeys = intermediate.keys.sorted { artist1, artist2 -> Bool in
                return artist1.name < artist2.name
            }
            
            for key in sortedKeys {
                groupedAlbums.append(intermediate[key]!)
            }
        }
    }
    
    fileprivate override func getAlbum(for indexPath: IndexPath) -> Album {
        return groupedAlbums[indexPath.section][indexPath.row]
    }
    
    // MARK: - UICollectionViewDataSource methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return groupedAlbums.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return groupedAlbums[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let artist = groupedAlbums[indexPath.section].first!.artist!
        
        var reusableView: UICollectionReusableView! = nil
        if kind == UICollectionView.elementKindSectionHeader {
            reusableView
                = collectionView
                    .dequeueReusableSupplementaryView(ofKind: kind,
                                                      withReuseIdentifier: "Header",
                                                      for: indexPath)
            if reusableView.subviews.count == 0 {
                reusableView.addSubview(UILabel(frame: reusableView.bounds))
            }
            
            let label = reusableView.subviews[0] as! UILabel
            label.text = artist.name
            label.textAlignment = .center
        }
        
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 30.0)
    }
}
