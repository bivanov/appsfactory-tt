//
//  SearchHistory.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/22/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import UIKit

fileprivate extension UserDefaults {
    func array<T>(for shKey: SearchHistory.SearchHistoryDefaultsKeys) -> [T]? {
        return array(forKey: shKey.rawValue) as? [T]
    }
    
    func set<T>(array: [T], for shKey: SearchHistory.SearchHistoryDefaultsKeys) {
        set(array, forKey: shKey.rawValue)
    }
}

class SearchHistory {
    enum SearchHistoryDefaultsKeys: String {
        case termsKey = "sh_terms_key"
    }
    
    private(set) var terms = Set<String>()
    
    private var defaults: UserDefaults {
        return UserDefaults.standard
    }
    
    func load() {
        if let array: [String] = defaults.array(for: .termsKey) {
            terms = Set<String>(array)
        }
    }
    
    private func save() {
        defaults.set(array: [String](terms), for: .termsKey)
    }
    
    func add(term: String) {
        terms.insert(term)
        save()
    }
    
    func similarTerms(to text: String) -> [String] {
        let lowercasedText = text.lowercased()
        let retVal = terms.compactMap { term -> String? in
            return term.lowercased().contains(lowercasedText) ? term : nil
        }
        
        return retVal
    }
}

protocol SearchHistoryControllerDelegate: class {
    func hintWasSelected(_ hint: String)
}

class SearchHistoryController: NSObject, UITableViewDelegate, UITableViewDataSource {

    var searchHistory = SearchHistory()
    private var matches = [String]()
    
    weak var delegate: SearchHistoryControllerDelegate?
    
    private let cellIdentifier = "SHUITableViewCell"
    unowned let tableView: UITableView
    
    init(with tableView: UITableView) {
        self.tableView = tableView
        
        super.init()
        
        searchHistory.load()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        
        self.tableView.alpha = 0.0
    }
    
    func refreshUi(for text: String) {
        matches = searchHistory.similarTerms(to: text)
        
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.tableView.alpha
                = (text.count > 2 && self.matches.count > 0) ? 1.0 : 0.0
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        matches.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        cell
            = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.backgroundColor = UIColor.beige
        
        cell!.textLabel?.text = matches[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        delegate?.hintWasSelected(matches[indexPath.row])
    }
    
}
