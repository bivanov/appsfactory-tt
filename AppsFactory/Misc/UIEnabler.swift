//
//  UIEnabler.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/21/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import UIKit

protocol UIEnabler {
    var viewsToInteract: [UIView] { get }
    func enableViews()
    func disableViews()
}

extension UIEnabler {
    func enableViews() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        for view in viewsToInteract {
            view.isUserInteractionEnabled = true
        }
    }
    
    func disableViews() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        for view in viewsToInteract {
            view.isUserInteractionEnabled = true
        }
    }
}
