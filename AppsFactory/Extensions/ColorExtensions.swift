//
//  ColorExtensions.swift
//  AppsFactory
//
//  Created by Bohdan Ivanov on 11/21/19.
//  Copyright © 2019 bivanov. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var gold: UIColor {
        return UIColor(named: "Gold")!
    }
    
    static var beige: UIColor {
        return UIColor(named: "Beige")!
    }
    
    static var wetAsphalt: UIColor {
        return UIColor(named: "WetAsphalt")!
    }
}
